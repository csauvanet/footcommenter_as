package fr.free.csauvanet.footballcomments;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class AboutActivity extends Activity {

    TextView aTxtView1 = null;
    TextView aTxtView2 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        aTxtView1 = (TextView) findViewById(R.id.view_about_area1);
        aTxtView2 = (TextView) findViewById(R.id.view_about_area2);

        String versionName = "App Version : " + getString(R.string.string_version_name);
        aTxtView1.setText(versionName);

        String sdkName = "Android SDK : " + Build.VERSION.SDK_INT + " (" + Build.VERSION.RELEASE + ")";
        aTxtView2.setText(sdkName);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // nothing special here
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // no action possible here
        return super.onOptionsItemSelected(item);
    }
}
