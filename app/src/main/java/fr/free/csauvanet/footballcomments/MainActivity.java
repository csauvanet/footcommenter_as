package fr.free.csauvanet.footballcomments;

import java.io.IOException;
import java.util.Locale;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends Activity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a {@link FragmentPagerAdapter}
     * derivative, which will keep every loaded fragment in memory. If this
     * becomes too memory intensive, it may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;
    PagerTabStrip mPagerStrip;

    public final static int FC_SETTINGS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mPagerStrip = (PagerTabStrip) findViewById(R.id.pager_tab_strip);
        mPagerStrip.setDrawFullUnderline(true);

        // for sounds
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            // start the preference activity
            Intent aIntentPref = new Intent();
            aIntentPref.setClass(MainActivity.this, PrefsActivity.class);
            startActivityForResult(aIntentPref, FC_SETTINGS);
            return true;
        }
        else if(id == R.id.action_black_box) {
            // 2 - displaying manu angryface + show me your warface
            MediaPlayer aMP = prepareAsyncMedialPlayer(this, R.raw.gueuledeguerrier_p1);
            aMP.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
            aMP.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    if(mp != null) {
                        // release
                        mp.reset();
                        mp.release();

                        // play war cry
                        MediaPlayer aMP = prepareAsyncMedialPlayer(getApplicationContext(), R.raw.gueuledeguerrier_p2);
                        aMP.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                // 1 - play sound
                                mp.start();

                                // 2 - display war face
                                Toast imgToast = new Toast(getApplicationContext());
                                LinearLayout toastLayout = new LinearLayout(getBaseContext());
                                toastLayout.setOrientation(LinearLayout.HORIZONTAL);
                                ImageView image = new ImageView(getBaseContext());
                                image.setImageResource(R.drawable.fmj_saiyan);
                                toastLayout.addView(image);
                                imgToast.setView(toastLayout);
                                imgToast.setDuration(Toast.LENGTH_SHORT);
                                imgToast.setGravity(Gravity.CENTER, 0, 0);
                                imgToast.show();
                            }
                        });
                        aMP.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                if (mp != null) {
                                    // release
                                    mp.reset();
                                    mp.release();
                                }
                            }
                        });
                    }
                }
            });
            return true;
        }
        else if(id == R.id.action_about) {
            // show about panel
            Intent aIntentPref = new Intent();
            aIntentPref.setClass(MainActivity.this, AboutActivity.class);
            startActivity(aIntentPref);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == FC_SETTINGS) {
            // refresh prefs
            loadPreferences();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // refresh prefs
        loadPreferences();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /*****************************************************************************
     * OTHER
     *****************************************************************************/

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class
            // below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
            case 0:
                return getString(R.string.title_section1).toUpperCase(l);
            case 1:
                return getString(R.string.title_section2).toUpperCase(l);
            case 2:
                return getString(R.string.title_section3).toUpperCase(l);
            case 3:
                return getString(R.string.title_section4).toUpperCase(l);
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private int sectionNumber = 0;  // 0 is no section associated, starts at 1

        // soundPool for all app
        private SoundPool _soundPool = null;  // 1 soundPool per fragment

        // commode long song
        private MediaPlayer mMediaPlayer_com = null;  // special one for the commode; > 5s
        private boolean isCommSoundRdy = false;  // special one for the commode; > 5s

        /**
         * Returns a new instance of this fragment for the given section number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }
        
        public PlaceholderFragment() {
            // Empty constructor, the section number will be retrieved from the Fragment arguments
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER);


            /*
            // block for API 21
            AudioAttributes.Builder audioAttrBuild = new AudioAttributes.Builder();
            audioAttrBuild.setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION);
            audioAttrBuild.setLegacyStreamType(AudioManager.STREAM_MUSIC);
            audioAttrBuild.setUsage(AudioAttributes.USAGE_GAME);
            SoundPool.Builder spBuilder = new SoundPool.Builder();
            spBuilder.setMaxStreams(8);
            spBuilder.setAudioAttributes(audioAttrBuild.build());
            */
            // deprecated but compatible with API 20-
            _soundPool = new SoundPool(8, AudioManager.STREAM_MUSIC, 0);
            // attach the onLoadCompleteListener, get the play rate
            _soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                @Override
                public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                    // play the sound when complete
                    _soundPool.play(sampleId, 1.0f, 1.0f, 0, 0, PrefsSingleton.getInstance().getPlayRate());
                }
            });
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            // create the medialPlayer only when activity is known
            if(mMediaPlayer_com == null) {

                // previous way: not optimized for UI thread - uses prepare() instead of prepareAsync
                //mMediaPlayer_com = MediaPlayer.create(activity, R.raw.commode);
                mMediaPlayer_com = prepareAsyncMedialPlayer(activity, R.raw.commode);
                mMediaPlayer_com.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        isCommSoundRdy = true;
                    }
                });
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView;
            setRetainInstance(true);
            switch(sectionNumber) {
            case 2:
                rootView = inflater.inflate(R.layout.fragment_tab2, container, false);
                Button btn9 = (Button) rootView.findViewById(R.id.button9);
                Button btn10 = (Button) rootView.findViewById(R.id.button10);
                Button btn11 = (Button) rootView.findViewById(R.id.button11);
                Button btn12 = (Button) rootView.findViewById(R.id.button12);
                Button btn13 = (Button) rootView.findViewById(R.id.button13);
                Button btn14 = (Button) rootView.findViewById(R.id.button14);
                Button btn15 = (Button) rootView.findViewById(R.id.button15);
                Button btn16 = (Button) rootView.findViewById(R.id.button16);
                btn9.setOnClickListener(aAllBtnListener);
                btn10.setOnClickListener(aAllBtnListener);
                btn11.setOnClickListener(aAllBtnListener);
                btn12.setOnClickListener(aAllBtnListener);
                btn13.setOnClickListener(aAllBtnListener);
                btn14.setOnClickListener(aAllBtnListener);
                btn15.setOnClickListener(aAllBtnListener);
                btn16.setOnClickListener(aAllBtnListener);
                break;
            case 3:
                rootView = inflater.inflate(R.layout.fragment_tab3, container, false);
                Button btn17 = (Button) rootView.findViewById(R.id.button17);
                Button btn18 = (Button) rootView.findViewById(R.id.button18);
                Button btn19 = (Button) rootView.findViewById(R.id.button19);
                Button btn20 = (Button) rootView.findViewById(R.id.button20);
                Button btn21 = (Button) rootView.findViewById(R.id.button21);
                Button btn22 = (Button) rootView.findViewById(R.id.button22);
                Button btn23 = (Button) rootView.findViewById(R.id.button23);
                Button btn24 = (Button) rootView.findViewById(R.id.button24);
                btn17.setOnClickListener(aAllBtnListener);
                btn18.setOnClickListener(aAllBtnListener);
                btn19.setOnClickListener(aAllBtnListener);
                btn20.setOnClickListener(aAllBtnListener);
                btn21.setOnClickListener(aAllBtnListener);
                btn22.setOnClickListener(aAllBtnListener);
                btn23.setOnClickListener(aAllBtnListener);
                btn24.setOnClickListener(aAllBtnListener);
                break;
            case 4:
                rootView = inflater.inflate(R.layout.fragment_tab4, container, false);
                Button btn25 = (Button) rootView.findViewById(R.id.button25);
                Button btn26 = (Button) rootView.findViewById(R.id.button26);
                Button btn27 = (Button) rootView.findViewById(R.id.button27);
                Button btn28 = (Button) rootView.findViewById(R.id.button28);
                Button btn29 = (Button) rootView.findViewById(R.id.button29);
                Button btn30 = (Button) rootView.findViewById(R.id.button30);
                Button btn31 = (Button) rootView.findViewById(R.id.button31);
                Button btn32 = (Button) rootView.findViewById(R.id.button32);
                btn25.setOnClickListener(aAllBtnListener);
                btn26.setOnClickListener(aAllBtnListener);
                btn27.setOnClickListener(aAllBtnListener);
                btn28.setEnabled(false);
                btn29.setEnabled(false);
                btn30.setEnabled(false);
                btn31.setEnabled(false);
                btn32.setEnabled(false);
                break;
            default:
                // case 1 also
                rootView = inflater.inflate(R.layout.fragment_main, container, false);
                Button btn1 = (Button) rootView.findViewById(R.id.button1);
                Button btn2 = (Button) rootView.findViewById(R.id.button2);
                Button btn3 = (Button) rootView.findViewById(R.id.button3);
                Button btn4 = (Button) rootView.findViewById(R.id.button4);
                Button btn5 = (Button) rootView.findViewById(R.id.button5);
                Button btn6 = (Button) rootView.findViewById(R.id.button6);
                Button btn7 = (Button) rootView.findViewById(R.id.button7);
                Button btn8 = (Button) rootView.findViewById(R.id.button8);
                btn1.setOnClickListener(aAllBtnListener);
                btn2.setOnClickListener(aAllBtnListener);
                btn3.setOnClickListener(aAllBtnListener);
                btn4.setOnClickListener(aAllBtnListener);
                btn5.setOnClickListener(aAllBtnListener);
                btn6.setOnClickListener(aAllBtnListener);
                btn7.setOnClickListener(aAllBtnListener);
                btn8.setOnClickListener(aAllBtnListener);
                break;
            }
            return rootView;
        }

        /**
         * since all button play a sound, the action is the same except the file played, the viewId
         * will tell which sound to play
         */
        OnClickListener aAllBtnListener = new OnClickListener() {

            @Override
            public void onClick(View v) {
                switch(v.getId()) {
                case R.id.button1:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.ceregard)).start();
                    break;
                case R.id.button2:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.ouhehe)).start();
                    break;
                case R.id.button3:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.ouais)).start();
                    break;
                case R.id.button4:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.tourniquet)).start();
                    break;
                case R.id.button5:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.diable)).start();
                    break;
                case R.id.button6:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.ahnonarbitre)).start();
                    break;
                case R.id.button7:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.pieddroid)).start();
                    break;
                case R.id.button8:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.questionmitps)).start();
                    break;
                case R.id.button9:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.arsene)).start();
                    break;
                case R.id.button10:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.aieaieaieaie)).start();
                    break;
                case R.id.button11:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.cestnul)).start();
                    break;
                case R.id.button12:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.phaseinteressante)).start();
                    break;
                case R.id.button13:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.chameau)).start();
                    break;
                case R.id.button14:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.dimtelefoot)).start();
                    break;
                case R.id.button15:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.bellesimages)).start();
                    break;
                case R.id.button16:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.alareguliere)).start();
                    break;
                case R.id.button17:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.tricoteur)).start();
                    break;
                case R.id.button18:
                    // this sound is more than 5s then should not be played by SoundPool (it will be cut)
                    if(PrefsSingleton.getInstance().getPlayRate() != 1.0f) {
                        // note: the error string should better be a string resource
                        Toast.makeText(getActivity(), "Désolé, la commode est trop rigide pour être déformée", Toast.LENGTH_LONG).show();
                    }
                    if(isCommSoundRdy) {
                        mMediaPlayer_com.start();
                    }
                    break;
                case R.id.button19:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.bonjour)).start();
                    break;
                case R.id.button20:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.buttransversale)).start();
                    break;
                case R.id.button21:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.concentrationpalpable)).start();
                    break;
                case R.id.button22:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.frimousse)).start();
                    break;
                case R.id.button23:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.franckjelasens)).start();
                    break;
                case R.id.button24:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.tulasens)).start();
                    break;
                case R.id.button25:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.epauleserpent)).start();
                    break;
                case R.id.button26:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.geomexparch)).start();
                    break;
                case R.id.button27:
                    new Thread(new SoundPoolRunnable(_soundPool, getActivity(), R.raw.humiliation)).start();
                    break;
                default:
                    if(BuildConfig.DEBUG) {
                        Log.i("FootComments Player", "A button clicked has no action associated: " + v.getId());
                    }
                    break;
                }
            }
        };

        @Override
        public void onDestroy() {
            super.onDestroy();
            // we need to release the _soundPool here
            if(_soundPool != null) {
                _soundPool.release();
                _soundPool = null;
            }
        }

        @Override
        public void onDetach() {
            super.onDetach();
            if(mMediaPlayer_com != null) {
                mMediaPlayer_com.stop();
                mMediaPlayer_com.reset();
                mMediaPlayer_com.release();
                mMediaPlayer_com = null;
            }
        }
    }

    /**
     * loads preferences from settings and update values accordingly
     * setting them into a singleton
     */
    protected void loadPreferences() {
        SharedPreferences myPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        // set the rate to the existing fragments
        String pRate = myPrefs.getString("pref_play_rate", "2");
        switch (pRate.charAt(0)) {
            case '1':
                PrefsSingleton.getInstance().setPlayRate(0.75f);
                break;
            case '3':
                PrefsSingleton.getInstance().setPlayRate(1.5f);
                break;
            default:
                PrefsSingleton.getInstance().setPlayRate(1.0f);
                break;
        }
    }

    /**
     * helper static method to get a MediaPlayer with prepareAsync() method (create() native method will call a prepare() method)
     * @param iContext activity where to find the resource
     * @param iRrcId resource ID to play (mp3 in raw)
     * @return the MediaPlayer ready to use, advised to register a OnPreparedListener on it
     */
    static MediaPlayer prepareAsyncMedialPlayer(Context iContext, int iRrcId) {
        MediaPlayer aMP = null;
        AssetFileDescriptor afd = iContext.getResources().openRawResourceFd(iRrcId);
        try {
            aMP = new MediaPlayer();
            aMP.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            aMP.setAudioStreamType(AudioManager.STREAM_MUSIC);
            aMP.prepareAsync();
        }
        catch(Exception e) {
            Log.e("FootCommenter error", "creating MediaPlayer for resource " + iRrcId + "Message: " + e.getMessage() );
        }
        if(afd!=null) {
            try {
                afd.close();
            }
            catch(IOException e) {
                if(BuildConfig.DEBUG) {
                    Log.d("FootComments", "exception" + e.getMessage());
                }
            }
        }
        return aMP;
    }
}
