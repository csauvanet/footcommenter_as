package fr.free.csauvanet.footballcomments;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.util.Log;

/**
 * Created by c.sauvanet on 1/23/15.
 * fragment for preferences display
 */
public class PrefsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
        ListPreference pref = (ListPreference) findPreference(getString(R.string.pref_play_rate));

        String toAppend = getSuffixForPrefSummary(pref.getValue());
        pref.setSummary(getString(R.string.pref_rate_sum) + toAppend);

        pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String toAppend = getSuffixForPrefSummary((String) newValue);
                preference.setSummary(getString(R.string.pref_rate_sum) + toAppend);
                return true;
            }
        });
    }

    /**
     * helper for building the preference interface
     * @param iValue the string value "1", "2" or "3"
     * @return the suffix to add in the menu
     */
    private String getSuffixForPrefSummary(String iValue) {
        String aReturnValue;
        switch(iValue.charAt(0)) {
            case '1':
                aReturnValue = " (slow)";
                break;
            case '3':
                aReturnValue = " (fast)";
                break;
            default:
                aReturnValue = "";
                break;
        }
        return aReturnValue;
    }
}
