package fr.free.csauvanet.footballcomments;

/**
 * Created by c.sauvanet on 1/23/15.
 */
public class PrefsSingleton {

    // global parameters that can be changed through settings
    private float mPlayRate = 1.0f;

    // Singleton
    private static PrefsSingleton aInstance = new PrefsSingleton();

    public static PrefsSingleton getInstance() {
        return aInstance;
    }

    private PrefsSingleton() {

    }

    // getters and setters

    /**
     * should be one of:
     * <ul>
     * <li>1.75f</li>
     * <li>1.0f</li>
     * <li>1.5f</li>
     * </ul>
     * @param iRate the playback rate value to set
     */
    public void setPlayRate(float iRate) {
        mPlayRate = iRate;
    }

    /**
     * Will be one of:
     * <ul>
     * <li>1.75f</li>
     * <li>1.0f</li>
     * <li>1.5f</li>
     * </ul>
     * @return the playback rate value
     */
    public float getPlayRate() {
        return mPlayRate;
    }
}
