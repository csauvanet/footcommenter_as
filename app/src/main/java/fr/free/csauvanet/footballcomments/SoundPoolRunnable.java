package fr.free.csauvanet.footballcomments;

import android.content.Context;
import android.media.SoundPool;
import android.util.Log;

/**
 * Dedicated to play sound clicked by user from another thread than UI thread (and its fragments)
 * Created by c.sauvanet on 1/30/15.
 */
public class SoundPoolRunnable implements Runnable {

    private SoundPool _sp;
    private Context _ctx;
    private int _soundId;

    public SoundPoolRunnable(SoundPool iSP, Context context, int resId) {
        _sp = iSP;
        _ctx = context;
        _soundId = resId;
    }

    @Override
    public void run() {

        // Moves the current Thread into the background, can increase UI Thread resources
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

        // just play the sound
        _sp.load(_ctx, _soundId, 1);

        // attach the onLoadCompleteListener, get the play rate
        _sp.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                // play the sound when complete
                _sp.play(sampleId, 1.0f, 1.0f, 0, 0, PrefsSingleton.getInstance().getPlayRate());
            }
        });
    }
}
